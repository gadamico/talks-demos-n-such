def custom_number_rounding(number):
    """ Takes in a number and separates out the whole and the fraction, and
        returns a custom rounded value.
         ...
    """
    try:
        assert type(number) in [int, float]
    except AssertionError:
        return False, 0
    if number == 0:
        return True, 0
    else:
        frac, whole = math.modf(number)
    if frac >= 0.8:
        number = int(whole) + 1
    elif frac < 0.8:
        number = int(whole)
    return True, number
