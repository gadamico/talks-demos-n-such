#!/usr/bin/env python
# -*- coding: utf: 8 -*-

"""
Reads in an Excel file returns a pivoted analysis.
See https://www.python-boilerplate.com/py3+executable/ for how to make alterations!
"""

import pandas as pd
import numpy as np
import sys

def make_catagorical_vars(x):
    if x < 205000:
        return "SMALL"
    elif  x <= 250000:
        return "MEDIUM"
    elif x <= 311000:
        return "BIG"
    else:
        return "HUGE"
    
def format_as_money(x):
    return "${:,.0f}".format(x)


if __name__ == "__main__":
    
    # Read in our data
    df = pd.read_excel('excel_data.xlsx')
    
    # For inspection
    print(df.head())  
    
    # Run whatever calculations
    df['Total'] = df['Jan'] + df['Feb'] + df['Mar']
    df['Sizes'] = df['Total'].apply(make_catagorical_vars)
    grouped_df = df[['Total', 'Sizes']].groupby('Sizes').sum() 
    
    # Melt and merge
    long_df = pd.melt(df, id_vars=['account'], value_vars=['Jan', 'Feb', 'Mar'])
    
    # Merge with our original df (we don't use it after this, but it's cool to see how it works)
    merged_df = pd.merge(df, long_df, on='account')
    
    # Reshape our long format into wide again
    temp_df = pd.pivot_table(long_df, index=['account'], columns='variable')
    temp_df.columns = temp_df.columns.droplevel().rename(None)
    temp_df.reset_index(inplace=True)
    
    # Format as money
    formatted_df = temp_df.applymap(format_as_money)
    
    # write out analyzed file
    formatted_df.to_csv('formatted.csv', index=False)
    print("we did it!")
